import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

public class Solution {

    /*
     * Complete the runningMedian function below.
     */
    static double[] runningMedian(int[] a) {
        /*
         * Write your code here.
         */
        
        double median = a[0];
        double ans[] = new double[a.length];
        ans[0] = a[0];
  
        
        PriorityQueue<Integer> smaller = new PriorityQueue<> 
        
        (Collections.reverseOrder()); 
        
        PriorityQueue<Integer> greater = new PriorityQueue<>(); 
          
        smaller.add(a[0]); 
          
        for(int i = 1; i < a.length; i++) 
        { 
              
            int x = a[i]; 
  
            if(smaller.size() > greater.size()) 
            { 
                if(x < median) 
                { 
                    greater.add(smaller.remove()); 
                    smaller.add(x); 
                } 
                else
                    greater.add(x); 
                median = (double)(smaller.peek() + greater.peek())/2;
            } 
   
            else if(smaller.size() == greater.size()) 
            { 
                if(x < median) 
                { 
                    smaller.add(x); 
                    median = (double)smaller.peek(); 
                } 
                else
                { 
                    greater.add(x); 
                    median = (double)greater.peek(); 
                } 
            } 
  
            else
            { 
                if(x > median) 
                { 
                    smaller.add(greater.remove()); 
                    greater.add(x); 
                } 
                else
                    smaller.add(x); 
                median = (double)(smaller.peek() + greater.peek())/2; 
                  
            } 
            ans[i] = median; 
        }
        return ans;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int aCount = Integer.parseInt(scanner.nextLine().trim());

        int[] a = new int[aCount];

        for (int aItr = 0; aItr < aCount; aItr++) {
            int aItem = Integer.parseInt(scanner.nextLine().trim());
            a[aItr] = aItem;
        }

        double[] result = runningMedian(a);

        for (int resultItr = 0; resultItr < result.length; resultItr++) {
            bufferedWriter.write(String.valueOf(result[resultItr]));

            if (resultItr != result.length - 1) {
                bufferedWriter.write("\n");
            }
        }

        bufferedWriter.newLine();

        bufferedWriter.close();
    }
}

