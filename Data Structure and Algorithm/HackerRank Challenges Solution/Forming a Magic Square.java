import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the formingMagicSquare function below.
    static int formingMagicSquare(int[][] s) {
        int m1[][]={{4,9,2},{3,5,7},{8,1,6}};
        int m2[][]={{8,3,4},{1,5,9},{6,7,2}};
        int m3[][]={{6,1,8},{7,5,3},{2,9,4}};
        int m4[][]={{2,7,6},{9,5,1},{4,3,8}};
        int t1=0,t2=0,t3=0,t4=0,t5=0,t6=0,t7=0,t8=0;
       
        for(int i=0;i<3;i++) {
            
            for(int j=0;j<3;j++){
                t1+=Math.abs(s[i][j]-m1[i][j]);
                t2+=Math.abs(s[i][j]-m1[i][2-j]);
                t3+=Math.abs(s[i][j]-m2[i][j]);
                t4+=Math.abs(s[i][j]-m2[i][2-j]);
                t5+=Math.abs(s[i][j]-m3[i][j]);
                t6+=Math.abs(s[i][j]-m3[i][2-j]);
                t7+=Math.abs(s[i][j]-m4[i][j]);
                t8+=Math.abs(s[i][j]-m4[i][2-j]);
            }
        }
        
        int answer=Math.min(Math.min(Math.min(s1,s2),Math.min(s3,s4)),Math.min(Math.min(s5,s6),Math.min(s7,s8)));
        return answer;

    }
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int[][] s = new int[3][3];

        for (int i = 0; i < 3; i++) {
            String[] sRowItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int j = 0; j < 3; j++) {
                int sItem = Integer.parseInt(sRowItems[j]);
                s[i][j] = sItem;
            }
        }
        int result = formingMagicSquare(s);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}

