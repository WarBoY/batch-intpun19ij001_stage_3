import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
       
        Stack<Integer> stack1 = new Stack<>();
        Stack<Integer> stack2 = new Stack<>();
        
        Scanner sc = new Scanner(System.in);
        
        int t = sc.nextInt();
        
        while(t>0){
           
            int type = sc.nextInt();
            if(type == 1){
                int input = sc.nextInt();
                stack1.push(input);
            } else {
                if(stack2.empty()){
                    while(!stack1.empty()){
                        stack2.push(stack1.pop());
                    }
                }
               
                if(type == 2)
                    stack2.pop();
                else 
                    System.out.println(stack2.peek());
            }
            t--;
        }
    }
}
