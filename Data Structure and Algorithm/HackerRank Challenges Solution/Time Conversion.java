import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

public class Solution {

    /*
     * Complete the timeConversion function below.
     */
    static String timeConversion(String s) {
        /*
         * Write your code here.
         */
          String st = "";
        if (s.substring(s.length() - 2).equals("AM")) {
            int n = Integer.valueOf(s.substring(0, 2));
            if (n == 12)
                st = st + "00" + s.substring(2, s.length() - 2);
            else
            st = s.substring(0, s.length() - 2);
        } else {
            int n = Integer.valueOf(s.substring(0, 2));
            n += 12;
            if(n != 24)
            st = st + Integer.toString(n) + s.substring(2, s.length() - 2);
            else
            st = s.substring(0, s.length() - 2);
        }
        return st;
    }

    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String s = scan.nextLine();

        String result = timeConversion(s);

        bw.write(result);
        bw.newLine();

        bw.close();
    }
}
